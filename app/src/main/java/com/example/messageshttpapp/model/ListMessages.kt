package com.example.messageshttpapp.model

import com.google.gson.annotations.SerializedName

data class ListMessages(
    @SerializedName("data")
    val messages : List<Message>
)

