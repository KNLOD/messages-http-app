package com.example.messageshttpapp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.messageshttpapp.model.Message

class MessageViewModel : ViewModel() {

    private var _message = MutableLiveData<Message>()
    val message : LiveData<Message>
    get() = _message

    private var _changedMessage = MutableLiveData<Message>()
    val changedMessage: LiveData<Message>
    get() = _changedMessage

    /**
     * set changedMessage to be observed in MainActivity
     * and then posted on Submit
     */
    fun changeMessage(message : Message){
        _changedMessage.value = message

    }

    /**
     * Set current message to be observed
     * for showing Dialog with this message's data
     */
    fun setCurrentMessage(message : Message){
        _message.value = message
    }
}