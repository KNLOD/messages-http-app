package com.example.messageshttpapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.example.messageshttpapp.model.ListMessages
import com.example.messageshttpapp.model.Message
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView: RecyclerView = findViewById(R.id.recycler_view)

        val viewModel: MessageViewModel by viewModels()

        val messages: MutableList<Message> = mutableListOf()
        lateinit var messageAdapter: MessageAdapter

        // Observer setting current message in Adapter
        // To show editing dialog for it
        viewModel.message.observe(this, Observer {
            val dialog = MessageFragment()
            dialog.show(supportFragmentManager, "edit Message")
        })


        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Companion.BASE_URL)
            .build()
            .create(ApiInterface::class.java)

        val retrofitData = retrofitBuilder.getMessages()
        // Get messages from URL
        retrofitData.enqueue(object : Callback<List<Message>?> {
            override fun onResponse(
                call: Call<List<Message>?>,
                response: Response<List<Message>?>
            ) {
                val responseBody = response.body()
                responseBody?.let {
                    // If received response
                    // Draw the recycler with item
                    messageAdapter = MessageAdapter(it, viewModel, applicationContext)
                    messageAdapter.notifyDataSetChanged()
                    recyclerView.adapter = messageAdapter

                    // Check messages in response
                    // add message to messages list
                    for (message in responseBody) {
                        Log.e("GET", "$message")
                        messages.add(message)
                    }
                }


            }

            override fun onFailure(call: Call<List<Message>?>, t: Throwable) {

            }
        })

        // Observing submiting changed message
        // And POST it
        viewModel.changedMessage.observe(this, Observer{ message ->
            messages.set(message.id.toInt() - 1, message)

            val retrofitPost = retrofitBuilder.postMessages(ListMessages(messages))
            retrofitPost.enqueue(object: Callback<ListMessages> {

                override fun onResponse(
                    call: Call<ListMessages>,
                    response: Response<ListMessages>
                ) {

                    val responseBody = response.body()
                    // Show the HTTP response to ensure that everything
                    // ran right
                    Toast.makeText(applicationContext, "${response.raw()}", Toast.LENGTH_LONG).show()
                    Log.e("POST", "posted")
                    Log.e("POST", "${response.raw()}, ${response.headers()}")


                    // Check posted Data
                    responseBody?.let {
                        for (message in responseBody.messages) {
                            Log.e("POST", "posted: $message")
                        }
                    }


                }

                override fun onFailure(call: Call<ListMessages>, t: Throwable) {

                    Log.e("POST", "$call, $t")

                }
            })
        })
    }


    companion object {
        const val BASE_URL = "https://jsonplaceholder.typicode.com/"
    }






    }

